from pydantic import BaseSettings

class Settings(BaseSettings):
    # python 3.4 Connection string, 3.6 or later is unuseable for passing phase issue
    connection_string : str = "mongodb://temporary_user:6DShJCzgc2xhmqc5@myadventure-cluster-shard-00-00.kkjdi.mongodb.net:27017,myadventure-cluster-shard-00-01.kkjdi.mongodb.net:27017,myadventure-cluster-shard-00-02.kkjdi.mongodb.net:27017/myadventure?ssl=true&replicaSet=atlas-r83ryx-shard-0&authSource=admin&retryWrites=true&w=majority"

settings = Settings()