from fastapi import FastAPI

from .routers import adventure

description = """
MyAdventureApp API helps you doing awesome task for your Adventure Story ✈

## Adventures
you will be able to 
* **Read all the adventures**
* **Read an adventure by id**
* **Create a new adventure**
* **Edit/Update existing adventure**
* **Delete an adventure by id**

"""

app = FastAPI(
    title = "MyAdventureAppAPI",
    description = description,
    contact = {
        "name" : "Salim Bin Usman",
        "url" : "https://linked.in/salim-bin-usman",
        "email" : "salimbinusman1@gmail.com"
    }
)

app.include_router(adventure.router)

@app.get("/")
async def home():
    return "Hello World, Welcome to FastAPI!"