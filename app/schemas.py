from typing import Optional
from datetime import date
from pydantic import BaseModel

class adventureBase(BaseModel):
    event : str
    time : date
    story : Optional[str] = None

class adventureOut(adventureBase):
    id : str

class adventureIn(adventureBase):
    id : Optional[str] = None


# Pydantic Model Decorator to show ObjectId from mongo
# https://github.com/tiangolo/fastapi/issues/1515#issue-630709452

# from pydantic import BaseModel, BaseConfig, Field
# from bson.objectid import ObjectId, InvalidId

# class OID(str):
#   @classmethod
#   def __get_validators__(cls):
#       yield cls.validate

#   @classmethod
#   def validate(cls, v):
#       try:
#           return ObjectId(str(v))
#       except InvalidId:
#           raise ValueError("Not a valid ObjectId")

# class MongoModel(BaseModel):

#   class Config(BaseConfig):
#       allow_population_by_field_name = True
#       json_encoders = {
#           ObjectId: lambda oid: str(oid),
#       }

#   @classmethod
#   def from_mongo(cls, data: dict):
#       """We must convert _id into "id". """
#       if not data:
#           return data
#       id = data.pop('_id', None)
#       return cls(**dict(data, id=id))

#   def mongo(self, **kwargs):
#       exclude_unset = kwargs.pop('exclude_unset', True)
#       by_alias = kwargs.pop('by_alias', True)

#       parsed = self.dict(
#           exclude_unset=exclude_unset,
#           by_alias=by_alias,
#           **kwargs,
#       )

#       # Mongo uses `_id` as default key. We should stick to that as well.
#       if '_id' not in parsed and 'id' in parsed:
#           parsed['_id'] = parsed.pop('id')

#       return parsed


    