from typing import List
from fastapi import APIRouter, status

from .. import crud, schemas

router = APIRouter(
    prefix="/adventures",
    tags=["adventures"]
)

# Function to convert "_id" from MongoDB to "id"
def create_id_key(adv):
    adv['id'] = str(adv['_id'])
    return adv

@router.get("/", response_model=List[schemas.adventureOut])
async def get_adventures():
    # Need to converted as a list to match response_model
    return [create_id_key(i) for i in crud.get_all_adventures()]

@router.get("/show", response_model=schemas.adventureOut)
async def get_adventure(id : str):
    return create_id_key(crud.get_an_adventure(id))

@router.post("/create", status_code = status.HTTP_201_CREATED)
async def create_adventure(adventure : schemas.adventureIn):
    # Convert ObjectId object from mongoDB as string to satisfy FastAPI return specs
    return str(crud.create_adventure(adventure))

@router.put("/update")
async def update_adventure(adventure : schemas.adventureIn):
    # Convert ObjectId object from mongoDB as string to satisfy FastAPI return specs
    crud.update_an_adventure(adventure)
    return {"One data updated successfully"}

@router.delete("/delete", response_model=schemas.adventureOut)
async def delete_adventure(id : str):
    return create_id_key(crud.delete_an_adventure(id))




