from fastapi.encoders import jsonable_encoder
from .. import crud, mongo, schemas

def get_test_data_id():
    data = mongo.client.myadventure.myadventure_collection.find_one({"event" : "A new event from test"})
    return str(data['_id'])

def get_test_data():
    test_data_id = get_test_data_id()
    test_data = schemas.adventureOut(**crud.get_an_adventure(test_data_id), id=test_data_id)
    return jsonable_encoder(test_data)
