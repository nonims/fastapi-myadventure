from fastapi.testclient import TestClient
from . import utils

def test_create_adventure(client : TestClient):
    response = client.post(
                    "/adventures/create",
                    json = {
                        "event": "A new event from test",
                        "time": "2021-08-06",
                        "story": "This is a story from test"
                    }
                )
    
    assert response.status_code == 201
    assert response.json() == utils.get_test_data_id()

def test_create_adventure_without_required_body(client : TestClient):
    response = client.post(
                    "/adventures/create",
                    json = {
                        "time": "2021-08-06",
                        "story": "This is a story from test"
                    }
                )
    
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
            "loc": [
                "body",
                "event"
            ],
            "msg": "field required",
            "type": "value_error.missing"
            }
        ]
    }

def test_get_adventures(client : TestClient):
    response = client.get("/adventures/")

    assert response.status_code == 200
    assert len(response.json()) > 0

def test_get_adventure(client : TestClient):
    test_data_id = utils.get_test_data_id()
    test_data = utils.get_test_data()

    response = client.get("/adventures/show?id="+test_data_id)
    assert response.json() == test_data

def test_get_adventure_without_id(client : TestClient):
    response = client.get("/adventures/show")
    assert response.status_code == 422
    assert response.json() == {
        "detail" : [
            {
                "loc" : [
                    "query",
                    "id"
                ],
                "msg" : "field required",
                "type" : "value_error.missing"
            }
        ]
    }

def test_update_adventure(client : TestClient):
    test_data_id = utils.get_test_data_id()
    response = client.put(
        "/adventures/update",
        json= {
            "id" : test_data_id,
            "event": "A new event from test",
            "time": "2021-08-06",
            "story": "This is a story from test-edited"
        }
    )
    
    assert response.status_code == 200
    assert response.json() == ["One data updated successfully"]

def test_update_adventure_without_required_body(client: TestClient):
    test_data_id = utils.get_test_data_id()
    response = client.put(
        "/adventures/update",
        json= {
            "id" : test_data_id,
            "time": "2021-08-06",
            "story": "This is a story from test"
        }
    )

    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
            "loc": [
                "body",
                "event"
            ],
            "msg": "field required",
            "type": "value_error.missing"
            }
        ]
    }

def test_delete_adventure(client : TestClient):
    test_data_id = utils.get_test_data_id()
    test_data = utils.get_test_data()
    response = client.delete("/adventures/delete?id="+test_data_id)

    assert response.status_code == 200
    assert response.json() == test_data

def test_delete_adventure_without_id(client : TestClient):
    response = client.delete("/adventures/delete")

    assert response.status_code == 422
    assert response.json() == {
        "detail" : [
            {
                "loc" : [
                    "query",
                    "id"
                ],
                "msg" : "field required",
                "type" : "value_error.missing"
            }
        ]
    }

