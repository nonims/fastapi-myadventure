from . import mongo, schemas
from fastapi.encoders import jsonable_encoder
from bson.objectid import ObjectId

def create_adventure(adventure : schemas.adventureIn):
    # FastAPI encoder to convert date datatype as string
    encoded_adventure = jsonable_encoder(adventure)
    inserted_data = { k:v for k,v in encoded_adventure.items() if v!=None}
    # return encoded_adventure
    return mongo.client.myadventure.myadventure_collection.insert_one(inserted_data).inserted_id

def get_all_adventures():
    return mongo.client.myadventure.myadventure_collection.find({})

def get_an_adventure(id : str):
    return mongo.client.myadventure.myadventure_collection.find_one({"_id" : ObjectId(id)})

def update_an_adventure(adventure : schemas.adventureIn):
    encoded_adventure = jsonable_encoder(adventure)
    updated_data = { k : v for k,v in encoded_adventure.items() if k!="id"}
    # return updated_data
    return mongo.client.myadventure.myadventure_collection.update_one({"_id" : ObjectId(encoded_adventure['id'])}, { "$set" : updated_data})

def delete_an_adventure(id : str):
    return mongo.client.myadventure.myadventure_collection.find_one_and_delete({"_id" : ObjectId(id)})
